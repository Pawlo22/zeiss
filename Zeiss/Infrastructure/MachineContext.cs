﻿using Microsoft.EntityFrameworkCore;
using Zeiss.Models;

namespace Zeiss.Infrastructure
{
    public class MachineContext : DbContext
    {
        public MachineContext(DbContextOptions<MachineContext> options)
            : base(options)
        { }

        public DbSet<Machine> Machines { get; set; }
        public DbSet<Event> Events { get; set; }
    }
}
