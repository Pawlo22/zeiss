﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Zeiss.Models;

namespace Zeiss.Infrastructure.Repositories
{
    public class EventsRepository : IEventsRepository
    {
        private readonly MachineContext _context;

        public EventsRepository(MachineContext context)
        {
            _context = context;
        }
        public async Task<Event> GetById(Guid eventId)
        {
            try
            {
                var ourEvent = await _context.Events.SingleOrDefaultAsync(q => q.Id == eventId);
                return ourEvent;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<Event>> GetAll()
        {
            try
            {
                var events = await _context.Events.ToListAsync();
                return events;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
