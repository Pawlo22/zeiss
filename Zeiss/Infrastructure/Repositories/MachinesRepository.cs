﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Zeiss.Models;

namespace Zeiss.Infrastructure.Repositories
{
    public class MachinesRepository : IMachinesRepository
    {
        private readonly MachineContext _context;

        public MachinesRepository(MachineContext context)
        {
            _context = context;
        }

        public async Task<Machine> GetById(Guid machineId)
        {
            try
            {
                var machine = await _context.Machines.Include(a =>a.Events).SingleOrDefaultAsync(q => q.Id == machineId);
                return machine;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<Machine>> GetAll()
        {
            try
            {
                var machines = await _context.Machines.Include(a => a.Events).ToListAsync();
                return machines;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task Add(Machine machine)
        {
            try
            {
                await _context.Machines.AddAsync(machine);
                await _context.SaveChangesAsync();
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task Update(Machine machine)
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<Guid> Remove(Machine machine)
        {
            try
            {
                _context.Machines.Remove(machine);
                await _context.SaveChangesAsync();
                return machine.Id;
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
