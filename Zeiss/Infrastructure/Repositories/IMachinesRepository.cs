﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zeiss.Models;

namespace Zeiss.Infrastructure.Repositories
{
    public interface IMachinesRepository
    {
        Task<Machine> GetById(Guid machineId);
        Task<IEnumerable<Machine>> GetAll();
        Task Add(Machine machine);
        Task Update(Machine machine);
        Task<Guid> Remove(Machine machine);
    }
}
