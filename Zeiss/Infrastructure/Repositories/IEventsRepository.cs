﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zeiss.Models;

namespace Zeiss.Infrastructure.Repositories
{
    public interface IEventsRepository
    {
        Task<Event> GetById(Guid eventId);
        Task<IEnumerable<Event>> GetAll();
    }
}
