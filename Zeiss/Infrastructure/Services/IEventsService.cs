﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zeiss.Models;

namespace Zeiss.Infrastructure.Services
{
    public interface IEventsService
    {
        Task AddEventAsync(EventLoad eventLoad);
    }
}
