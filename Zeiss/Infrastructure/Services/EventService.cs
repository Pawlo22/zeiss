﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zeiss.Infrastructure.Repositories;
using Zeiss.Models;

namespace Zeiss.Infrastructure.Services
{
    public class EventService : IEventsService
    {
        private readonly IMachinesRepository _machinesRepository;
        public EventService(IMachinesRepository machinesRepository)
        {
            _machinesRepository = machinesRepository;
        }
        public async Task AddEventAsync(EventLoad eventLoad)
        {
            var machineId = eventLoad.Payload.MachineId;
            var machine = await _machinesRepository.GetById(machineId);

            if (machine == null)
            {
               await AddNewMachine(eventLoad.Payload);
            }
            else
            {
                await AddEventToMachine(machine, eventLoad.Payload);
            }
        }

        private async Task AddEventToMachine(Machine machine, Event payload)
        {
            var newEvent = new Event()
            { Id = payload.Id, MachineId = payload.MachineId, Status = payload.Status, TimeStamp = payload.TimeStamp };
            machine.Events.Add(newEvent);
            await _machinesRepository.Update(machine);
        }

        private async Task AddNewMachine(Event payload)
        {
            var newEvent = new Event()
            { Id = payload.Id, MachineId = payload.MachineId, Status = payload.Status, TimeStamp = payload.TimeStamp };
            var newMachine = new Machine()
            {
                Id = payload.MachineId,
                Status = payload.Status,
                Events = new List<Event>()
                {
                    newEvent
                }
            };
            await _machinesRepository.Add(newMachine);
        }
    }
}
