﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeiss
{
    public class ZeissSettings
    {
        public string ConnectionString { get; set; }
        public string WebSocketUri { get; set; }
    }
}
