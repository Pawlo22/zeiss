﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeiss.Models
{
    public class Machine
    {
        public Guid Id { get; set; }
        public Status Status { get; set; }
        public List<Event> Events { get; set; }

        public Machine()
        {
            Events = new List<Event>();
        }
    }
}
