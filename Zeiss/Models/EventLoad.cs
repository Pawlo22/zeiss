﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeiss.Models
{
    public class EventLoad
    {
        public string Topic { get; set; }
        public string Ref { get; set; }
        public Event Payload { get; set; }
        public string Event { get; set; }
    }
}
