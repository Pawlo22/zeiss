﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeiss.Models
{
    public enum Status
    {
        Idle,
        Running,
        Finished,
        Errored,
        Repaired
    }
}
