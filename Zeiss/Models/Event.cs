﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Zeiss.Models
{
    public class Event
    {
        public Guid Id { get; set; }
        [JsonProperty(PropertyName = "machine_id")]
        public Guid MachineId { get; set; }
        public Status Status { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
