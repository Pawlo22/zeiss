﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Zeiss.Infrastructure.Services;
using Zeiss.Models;
using Machine = System.Reflection.PortableExecutable.Machine;

namespace Zeiss.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StreamController : ControllerBase
    {
        private readonly IEventsService _eventsService;
        private readonly IConfiguration _configuration;
        public StreamController(IEventsService eventsService, IConfiguration configuration)
        {
            _eventsService = eventsService;
            _configuration = configuration;
        }
        // GET: api/Stream
        [HttpGet]
        public async Task<ActionResult> GetStream()
        {
            using (ClientWebSocket ws = new ClientWebSocket())
            {
                Uri serverUri = new Uri(_configuration["WebSocketUri"]);
                await ws.ConnectAsync(serverUri, CancellationToken.None);
                while (ws.State == WebSocketState.Open)
                {
                    ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[1024]);
                    WebSocketReceiveResult result = await ws.ReceiveAsync(bytesReceived, CancellationToken.None);
                    var stringToConvert= Encoding.UTF8.GetString(bytesReceived.Array, 0, result.Count);
                    Console.WriteLine(stringToConvert); //debugging purposes
                    EventLoad eventLoadResult = Newtonsoft.Json.JsonConvert.DeserializeObject<EventLoad>(stringToConvert);
                    await _eventsService.AddEventAsync(eventLoadResult);
                    if (ws.State == WebSocketState.Closed)
                    {
                        return Ok("Connection closed");
                    }
                }

                return BadRequest("Something went wrong");
            }

        }
    }
}