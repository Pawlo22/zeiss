﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Zeiss.Infrastructure;
using Zeiss.Infrastructure.Repositories;
using Zeiss.Models;

namespace Zeiss.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MachinesController : ControllerBase
    {
        private readonly IMachinesRepository _machinesRepository;

        public MachinesController(IMachinesRepository machinesRepository)
        {
            _machinesRepository = machinesRepository;
        }

        // GET: api/Machines
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Machine>>> GetMachines()
        {
            var machines = await _machinesRepository.GetAll();
            return Ok(machines);
        }

        // GET: api/Machines/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Machine>> GetMachine(Guid id)
        {
            var machine = await _machinesRepository.GetById(id);

            if (machine == null)
            {
                return NotFound();
            }

            return Ok(machine);
        }


        // POST: api/Machines
        [HttpPost]
        public async Task<ActionResult<Machine>> PostMachine(Machine machine)
        {
            try
            {
                await _machinesRepository.Add(machine);
                return CreatedAtAction("PostMachine", new { id = machine.Id }, machine);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Unable to save changes. " +
                                             "Try again, and if the problem persists " +
                                             "see your system administrator.");
                return BadRequest(ModelState);
            }
        }

        // DELETE: api/Machines/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Machine>> DeleteMachine(Guid id)
        {
            var machineToRemove = await _machinesRepository.GetById(id);

            if (machineToRemove == null)
            {
                return NotFound();
            }

            var machineRemovedId = await _machinesRepository.Remove(machineToRemove);
            return Ok(machineRemovedId);
        }
    }
}
